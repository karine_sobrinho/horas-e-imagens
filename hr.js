function carregar(){
    var msg= document.getElementById('msg')
    var img= document.getElementById('imagem')
    var data = new Date()
    var hora = data.getHours()
    
    msg.innerHTML = `Agora são ${hora} horas`
    if (hora>= 4 && hora <=12){
        //Bom dia!
        img.src = 'dia.jpg'
        document.body.style.background= '#FFA500'
    } else if (hora>=12 && hora<=18){
        //Boa tarde!
        img.src = 'tarde.jpg'
        document.body.style.background = '#DAA520'
    } else{
        //Boa noite!
        img.src = 'noite.jpg'
        document.body.style.background = '#4682B4'
    }

}